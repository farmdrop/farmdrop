from dateutil.parser import parse
from django.contrib import messages
from django.views.generic import DetailView, ListView, TemplateView, View
from django.core.urlresolvers import reverse
from django.db.models import Q

from braces.views import LoginRequiredMixin

from oscar.apps.order.models import Order
from oscar.apps.partner.models import Partner

from reporter.utils import get_products_from_orders, load_user_partners



class ProductReportView(LoginRequiredMixin, ListView):
    """ Presents a list of orders with various options for filtering """
    template_name = 'reporter/order-report.html'
    context_object_name = 'orders'
    model = Order

    def get_queryset(self):
        objs = super(ProductReportView, self).get_queryset()
        return objs

    def get_context_data(self, **kwargs):
        context = super(ProductReportView, self).get_context_data(**kwargs)

        from_date = self.request.GET.get('from_date')
        to_date = self.request.GET.get('to_date')
        partner_code = self.request.GET.get('partner')
        if from_date:
            from_date = parse(from_date)
        if to_date:
            to_date = parse(to_date)

        context = load_user_partners(self.request.user, context, partner_code)
        if context['partners']:
            context['products'] = get_products_from_orders(
                self.object_list,
                context['partners'],
                from_date=from_date,
                to_date=to_date
            )
        return context


class ThisWeekReportView(LoginRequiredMixin, TemplateView):
    """ Basic docstring

    """
    template_name = 'reporter/this-week-report.html'

    #def get_queryset(self):
    #    if self.request.user.is_authenticated():
    #        return Subscription.objects.filter(user=self.request.user)
    #    else:
    #        return Subscription.objects.none()
