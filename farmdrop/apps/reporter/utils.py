from django.utils.translation import ugettext_lazy as _
from django.db.models import Q


def get_products_from_orders(orders, partners, from_date=None, to_date=None):
    """
    Given a list of orders and partners, grab all orders belonging to the given
    partner.

    Returns a dictionary of products with quantities, product name and sku

    """
    products = []
    if from_date:
        orders = orders.filter(date_placed__gt=from_date)
    if to_date:
        orders = orders.filter(date_placed__lte=to_date)

    for order in orders:
        for line in order.lines.all():
            if line.partner in partners:
                try:
                    existing = [p['product'] for p in products
                                if p['product'] == line.product][0]
                except IndexError:
                    existing = None

                if existing:
                    for product in products:
                        if product['product'] == existing:
                            product['quantity'] += line.quantity
                else:
                    new_product = {
                        'product': line.product,
                        'quantity': line.quantity,
                        'sku': line.stockrecord.partner_sku
                    }
                    products.append(new_product)
    return products


def load_user_partners(user, context, partner_code=None):
    """
    Given a user, context object and an optional partner code

    Returns context loaded with partners for the user, or all
    partners if the user is a staff member. Also loads the partner
    context variable with a given partner code, if available.
    """
    context['partners'] = []

    if user.is_staff:
        # Default for staff user is all partners with codes.
        query = Q(code__isnull=False)
        from oscar.apps.partner.models import Partner
        if partner_code:
            # If we're filtering by partner code, add it to the query
            query = Q(code=partner_code)
            context['partner'] = Partner.objects.filter(query).first()
        # Load context variable with partners in the query
        context['partners'] = [p for p in Partner.objects.filter(query)]

    elif user.is_authenticated():
        # Load the context variable with 
        context['partners'] = [p for p in user.partners.all()]

    return context

def inject_new_fulfilment_menu(dashboard):
    for idx, item in enumerate(dashboard):
        if item['icon'] == 'icon-shopping-cart':
            pass
            #dashboard.remove(dashboard[idx])

    dashboard += [
        {
            'label': _('Products ordered'),
            'icon': 'icon-shopping-cart',
            'children': [
                #{
                #    'label': _('Orders'),
                #    'url_name': 'dashboard:order-list',
                #},
                {
                    'label': 'Products ordered',
                    'url_name': 'reporter:product-report',
                },
                #{
                #    'label': _('Statistics'),
                #    'url_name': 'dashboard:order-stats',
                #},
                #{
                #    'label': _('Partners'),
                #    'url_name': 'dashboard:partner-list',
                #},
                # The shipping method dashboard is disabled by default as it might
                # be confusing. Weight-based shipping methods aren't hooked into
                # the shipping repository by default (as it would make
                # customising the repository slightly more difficult).
                # {
                #     'label': _('Shipping charges'),
                #     'url_name': 'dashboard:shipping-method-list',
                # },
            ]
        }
    ]
    return dashboard

def report_access(user, url_name, url_args=None, url_kwargs=None):
    return True
