from django.test import TestCase
from django.contrib.auth.models import User

from .utils import get_products_from_orders, load_user_partners

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class ReporterViewTests(TestCase):
    fixtures = ['reporter.json',
                'auth.json',
                'oscar.json']

    def test_product_report_view(self):

        url = reverse('reporter:product-report')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)

        #response = self.client.post("/my/form/", {"data": "value"})
        #self.assertEqual(response.status_code, 302) # Redirect on form success

        #response = self.client.post("/my/form/", {})
        #self.assertEqual(response.status_code, 200) # we get our page back with an error

    def test_load_user_partners(self):

        user = User.objects.first()
        context = load_user_partners(user, {})
        self.assertEqual(context['partners'], [])

