from django.conf.urls import patterns, url, include
from reporter.dashboard import views

urlpatterns = [
    url(r'^orders/reports/products/$', views.ProductReportView.as_view(), name='product-report'),
    url(r'^orders/reports/this-week/$', views.ThisWeekReportView.as_view(), name='this-week-report'),
]
